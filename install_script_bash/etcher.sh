#!/bin/bash
set -e

echo "Etcher:"
echo "deb https://deb.etcher.io stable etcher" | tee /etc/apt/sources.list.d/balena-etcher.list
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 379CE192D401AB61
wget -O- "http://keyserver.ubuntu.com/pks/lookup?op=get&search=0x379CE192D401AB61" | apt-key add -
sudo apt-get update
sudo apt-get install balena-etcher-electron -y
echo "Etcher Installed"
echo ''
