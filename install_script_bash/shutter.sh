#!/bin/bash
set -e

echo "Install Shutter :"
sudo add-apt-repository -y ppa:linuxuprising/shutter
sudo apt install shutter -y
echo "Install shutter tools :"
wget https://launchpad.net/ubuntu/+archive/primary/+files/libgoocanvas-common_1.0.0-1_all.deb -O libgoocanvas-common_1.0.0-1_all.deb
wget https://launchpad.net/ubuntu/+archive/primary/+files/libgoocanvas3_1.0.0-1_amd64.deb -O libgoocanvas3_1.0.0-1_amd64.deb
wget https://launchpad.net/ubuntu/+archive/primary/+files/libgoo-canvas-perl_0.06-2ubuntu3_amd64.deb -O libgoo-canvas-perl_0.06-2ubuntu3_amd64.deb
echo "* Install dpkg"
sudo dpkg -i libgoocanvas-common_1.0.0-1_all.deb
sudo dpkg -i libgoocanvas3_1.0.0-1_amd64.deb
sudo dpkg -i libgoo-canvas-perl_0.06-2ubuntu3_amd64.deb
rm libgoocanvas-common_1.0.0-1_all.deb
rm libgoocanvas3_1.0.0-1_amd64.deb
rm libgoo-canvas-perl_0.06-2ubuntu3_amd64.deb
echo "Shutter Installed"
echo ''
