#!/bin/bash
set -e

echo "Install Peek :"
sudo add-apt-repository ppa:peek-developers/stable -y
sudo apt update
sudo apt install peek -y
echo "Peek Installed"
echo ''
