#!/bin/bash
set -e

echo "Install powerlevel9k font:"
wget https://github.com/powerline/powerline/raw/develop/font/PowerlineSymbols.otf -O PowerlineSymbols.otf
wget https://github.com/powerline/powerline/raw/develop/font/10-powerline-symbols.conf -O 10-powerline-symbols.conf
mkdir -p $HOME/.local/share/fonts
mv PowerlineSymbols.otf $HOME/.local/share/fonts/
fc-cache -vf $HOME/.local/share/fonts/
mkdir -p $HOME/.config/fontconfig/conf.d
mv 10-powerline-symbols.conf $HOME/.config/fontconfig/conf.d/
echo "powerlevel9k Fonts Installed"
echo ''
