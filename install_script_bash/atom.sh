#!/bin/bash
set -e

echo "Install Atom :"
wget https://atom.io/download/deb -O atom.deb
echo "* Install dpkg"
sudo dpkg -i atom.deb
rm atom.deb
echo "Atom Installed"
echo ''
