#!/bin/bash
set -e

echo "Install teamviewer :"
wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb -O teamviewer_amd64.deb
echo "* Install dpkg"
sudo dpkg -i teamviewer_amd64.deb
rm teamviewer_amd64.deb
echo "Teamviewer Installed"
echo ''
