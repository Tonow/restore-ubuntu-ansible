# Ansible-playbook


## Install on Ubuntu

```bash
sudo apt-get install software-properties-common \
&& sudo apt-add-repository ppa:ansible/ansible \
&& sudo apt-get update \
&& sudo apt-get install ansible
```

## Documentation

* [Config playbook](https://www.linode.com/docs/applications/configuration-management/learn-how-to-install-ansible-and-run-playbooks/#ansible-configuration-via-playbooks)

* [Official playbook doc](https://docs.ansible.com/ansible/latest/user_guide/playbooks.html)

* [Official apt-get doc](https://docs.ansible.com/ansible/latest/modules/apt_module.html)

* [tutorialspoint doc](https://www.tutorialspoint.com/ansible/ansible_playbooks.htm)


## Running

* ```ansible-playbook restore-new-deb.yml```

## Auto-deployment

```bash
wget https://gitlab.com/Tonow/restore-ubuntu-ansible/-/raw/perso/standalone.sh\?inline\=false -O $HOME/.standalone.sh \
&& echo "alias python='python3'" >> $HOME/.bashrc \
&& source $HOME/.bashrc \
&& sh $HOME/.standalone.sh \
&& rm $HOME/.standalone.sh
```
