#!/bin/bash
set -e

echo "\nInstall ansible\n"
sudo apt-get update
sudo apt-get install software-properties-common -y
sudo apt-get install ansible -y

echo "\nInstall git"
sudo apt-get install git

echo "\nclone repo Tonow/restore-ubuntu-ansible"
git clone https://gitlab.com/Tonow/restore-ubuntu-ansible.git .restore-ubuntu-ansible

echo "\nRun ansible script for perso setting"
cd .restore-ubuntu-ansible
git checkout perso
sed -i "s/HOST/$HOST/g" host_vars/localhost.yml
sed -i "s/USER/$USER/g" host_vars/localhost.yml
sudo ansible-playbook restore-new-deb.yml
sudo apt-get update
sudo ./manage_script_install.py

tput setaf 2
tput bold
echo "\n###################################\n###################################"
echo "Install finished !"
echo "After ZSH install you can do:"
echo "git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k"
echo "cp -r dotfile $HOME/dotfile"
echo "\n###################################\n###################################"
tput sgr0;
echo ''
echo "Install Oh-my-szh: doc on https://doc.ubuntu-fr.org/zsh"
sudo -u $USER sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
